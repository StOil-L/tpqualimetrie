package lpuccio.TPQualimetrie;

public class CourrielTitreVide extends Exception{
	
	public CourrielTitreVide(String string) {
		super(string);
	}
}
