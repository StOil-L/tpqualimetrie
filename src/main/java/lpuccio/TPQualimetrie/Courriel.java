package lpuccio.TPQualimetrie;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class Courriel {
	private String dest;
	private String titre;
	private String corps;
	private ArrayList<String> pj;
	
	public Courriel (String dest, String titre, String corps) throws CourrielAdresseInvalide, CourrielTitreVide {
		if (!Pattern.matches("[a-z0-9.]+@[a-z]+[.][a-z]+", dest)) {
			throw new CourrielAdresseInvalide("Adresse de destination invalide.");
		}
		else if (titre.equals("")){
			throw new CourrielTitreVide("Le titre du courriel est vide.");
		}
		else {
			this.dest = dest;
			this.titre = titre;
			this.corps = corps;
			this.pj = new ArrayList<>();
		}
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) throws CourrielAdresseInvalide {
		if (!Pattern.matches("[a-z0-9.]+@[a-z]+[.][a-z]+", dest)) {
			throw new CourrielAdresseInvalide("Adresse de destination invalide.");
		}
		else {
			this.dest = dest;
		}
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) throws CourrielTitreVide {
		if ("".equals(titre)) {
			throw new CourrielTitreVide("Le titre du courriel est vide.");
		}
		else {
			this.titre = titre;
		}
	}

	public String getCorps() {
		return corps;
	}

	public void setCorps(String corps) {
		this.corps = corps;
	}
	
	public int getNbPJ() {
		return this.pj.size();
	}
	
	public void addPieceJointe(String cheminpj) {
		if (!this.pj.contains(cheminpj)){
			this.pj.add(cheminpj);
		}
	}
	
	public void removePieceJointe(String cheminpj) {
		if (this.pj.contains(cheminpj)){
			this.pj.remove(cheminpj);
		}
	}
	
	public boolean checkPJ() throws CourrielAucunePJ{
		if(this.corps.contains("PJ") || this.corps.contains("joint")) {
			if(this.pj.isEmpty()) {
				throw new CourrielAucunePJ("Il manque une pièce jointe au mail.");
			}
			else {
				return true;
			}
		}
		else {
			return false;
		}
	}
}
