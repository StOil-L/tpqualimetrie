package lpuccio.TPQualimetrie;

public class CourrielAdresseInvalide extends Exception{
	
	public CourrielAdresseInvalide(String string) {
		super(string);
	}
}
