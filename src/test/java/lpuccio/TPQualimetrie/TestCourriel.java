package lpuccio.TPQualimetrie;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
//import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

class TestCourriel {
	
	private String path = "D:\\Documents\\Travail\\hai501i\\explication.txt";
	private String path2 = "D:\\Documents\\Travail\\hai501i\\argument.txt";
	
	Courriel c1;
	
	@BeforeEach
	void setUp() throws CourrielAdresseInvalide, CourrielTitreVide {
		c1 = new Courriel("lorenzo@umontpellier.fr", "HAI501I", "Ci joint une explication pour ne pas nous donner de TP");
	}
	
	@ParameterizedTest(name = "testAdresseInvalide n°{index}: {0} - {1}")
	@MethodSource("streamAdresseInvalide")
	void testAdresseInvalide(String address, String reason, Courriel c2) {
		assertThrows(CourrielAdresseInvalide.class, () -> {
			new Courriel(address, "HAI501", "Ne nous donnez pas de TP svp");
		});
		assertThrows(CourrielAdresseInvalide.class, () -> {
			c2.setDest(address);
		});
	}
	
	private static Stream<Arguments> streamAdresseInvalide() throws CourrielAdresseInvalide, CourrielTitreVide {
		Courriel c2 = new Courriel("lorenzo@umontpellier.fr", "HAI501", "Ne nous donnez pas de TP svp");
		return Stream.of(
					Arguments.of("beaaanswtf", "mauvais regex: [a-z0-9.]+", c2),
					Arguments.of("onestla@", "mauvais regex: [a-z0-9.]+@", c2),
					Arguments.of("@wesh", "mauvais regex: @[a-z]+", c2),
					Arguments.of("bruh@lmao", "mauvais regex: [a-z0-9.]+@[a-z]+", c2),
					Arguments.of("lorénzo@umontpellier.fr", "mauvais regex: [a-z0-9à-ú.]+@[a-z]+[.][a-z]+", c2)
						);
	}
	
	@Test
	void testTitreVide() {
		assertThrows(CourrielTitreVide.class, () -> {
			new Courriel("lorenzo@umontpellier.fr", "", "Ne nous donnez pas de TP svp");
		});
		assertThrows(CourrielTitreVide.class, () -> {
			c1.setTitre("");
		});
	}
	
	@Test
	void testAucunePJ() {
		assertThrows(CourrielAucunePJ.class, () -> {
			c1.checkPJ();
		});
		c1.setCorps("Ci PJ une explication pour ne pas nous donner de TP");
		assertThrows(CourrielAucunePJ.class, () -> {
			c1.checkPJ();
		});
		c1.setCorps("Ci joint PJ une explication pour ne pas nous donner de TP");
		assertThrows(CourrielAucunePJ.class, () -> {
			c1.checkPJ();
		});
		c1.setCorps("Ci PJ joint une explication pour ne pas nous donner de TP");
		assertThrows(CourrielAucunePJ.class, () -> {
			c1.checkPJ();
		});
	}
	
	@Test
	void testAddRemovePieceJointe() {
		//1er test add: on ajoute l'élément path
		c1.addPieceJointe(path);
		assertEquals(1, c1.getNbPJ());
		//2ème test add: on vérifie que rien n'a bougé après avoir tenté d'ajouter path une deuxième fois
		c1.addPieceJointe(path);
		assertEquals(1, c1.getNbPJ());
		//1er test remove: on vérifie que rien ne se passe si l'on cherche à retirer un élément qui n'est pas dans PJ
		c1.removePieceJointe(path2);
		assertEquals(1, c1.getNbPJ());
		//2ème test remove: on retire l'élément path
		c1.removePieceJointe(path);
		assertEquals(0, c1.getNbPJ());
		//3ème test remove: on vérifie que rien ne se passe si pj est vide
		c1.removePieceJointe(path);
		assertEquals(0, c1.getNbPJ());
	}
	
	@Test
	void testGetSetDest() throws CourrielAdresseInvalide {
		assertEquals("lorenzo@umontpellier.fr", c1.getDest());
		c1.setDest("jeanmarc.puccio@gmail.com");
		assertEquals("jeanmarc.puccio@gmail.com", c1.getDest());
	}
	
	@Test
	void testGetSetTitre() throws CourrielTitreVide {
		assertEquals("HAI501I", c1.getTitre());
		c1.setTitre("Repas avec Patricia");
		assertEquals("Repas avec Patricia", c1.getTitre());
	}
	
	@Test
	void testGetSetCorps() {
		assertEquals("Ci joint une explication pour ne pas nous donner de TP", c1.getCorps());
		c1.setCorps("Que penses-tu de 12h30 au bistrot de Guermantes?");
		assertEquals("Que penses-tu de 12h30 au bistrot de Guermantes?", c1.getCorps());
	}
	
	@Test
	void testCheckPJ() throws CourrielAucunePJ {
		c1.addPieceJointe(path);
		assertEquals(true, c1.checkPJ());
		c1.setCorps("Que penses-tu de 12h30 au bistrot de Guermantes?");
		assertEquals(false, c1.checkPJ());
		c1.removePieceJointe(path);
		assertEquals(false, c1.checkPJ());
	}
}
